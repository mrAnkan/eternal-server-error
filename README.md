# AN ETERNAL SERVER ERROR HAS OCCURRED
This site is an tribute to the classic joke of *Pachydermic Personnel Prediction, the bold new proposal for matching high-technology people and professions*. The joke or story can be read as a whole at [https://www-users.cs.york.ac.uk/susan/joke/elephant.htm](https://www-users.cs.york.ac.uk/susan/joke/elephant.htm)

Visit this tribute site at [https://eternalservererror.com/](https://eternalservererror.com/)

## Key features
* A tribute reference to classic joke
* An handcrafted Elephant using CSS
* Dark UI with Cool fonts

![alt text](img/eternalservererror.com.png "A screenshot of the site")
